# Montant Lucas vu3


## Lien vers l'api 

L'api publique permet de récupérer des infos sur des "amiibo", des petites figurines Nintendo.

La documentation de l'api

Il y a deux endpoints importants: récupérer une liste d'amiibo et récupérer les détails d'un amiibo

Récupérer tous les amiibos

https://www.amiiboapi.com/api/amiibo/

📌L'id des amiibo est le "tail"

https://www.amiiboapi.com/api/amiibo/?tail=01610502


Import des assets statics: 3pts 👍 (une asset sur la page d'accueil)

Gestion des routes: 3pts 👍 (router/index.js)

Découpage composants: 3pts 👍 (1 seule composant => component/AmiiboComponent)

Récupération data from api: 6pts 👍 (Toutes les pages : pages d'accueil 3 amiibo / page amiibos => tous les amiibos / amiibo:id => detail de l'amiibo)

Affichage des données de l'api: 3pts 👍 (Toutes les pages)

Nommage, clean code, logique: 2pts 