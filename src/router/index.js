import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import AmiiboList from "../views/AmiiboList.vue";
import AmiiboDetails from "../views/AmiiboDetails.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/amiibos",
      name: "amiiboList",
      component: AmiiboList,
    },
    {
      path: "/amiibo/:tail",
      name: "amiibo",
      component: AmiiboDetails,
    },
  ],
});

export default router;